import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { Card } from 'antd';

const User = () => {
    const param = useParams()
    const [userId, setuserId]:any = useState();
useEffect(()=>{
 if(param.id){
     setuserId(param.id)
 }
},[param])
    return (
        <div className="site-card-border-less-wrapper">
        <Card title={`User${userId}`} bordered={false} style={{ width: 300 }}>
          <p>Some content</p>
          
        </Card>
      </div>
    )
}

export default User
