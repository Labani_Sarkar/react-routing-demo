import { Col, Row } from 'antd'
import React from 'react'
import { Link, Routes, Route } from 'react-router-dom'
import User from './User'

const Profile = () => {
    return (
        <div className="div-content">
             <h1 > Profile details</h1>
             <h3>Routing with param example:</h3>
           Go to Profile for :
           <Row><Col md={6}><Link to="/dashboard/profile/user/1">User 1</Link></Col><Col md={6}>
               <Link to="/dashboard/profile/user/2">User 2</Link></Col><Col md={6}>
                   <Link to="/dashboard/profile/user/3">User 3</Link></Col><Col md={6}>
                       <Link to="/dashboard/profile/user/4">User 4</Link></Col></Row>
                       <Routes> <Route path="/user/:id" element={<User />} />  </Routes>
        </div>
    )
}

export default Profile
