import { Button, Col, Row } from 'antd';
import React from 'react'
import { Link, Routes, Route } from "react-router-dom";
import 'antd/dist/antd.css';
import Profile from './Profile';
import MenuPage from './Menu';
const Dashboard = () => {
    return (
       
        <div className="div-content">
        <h1 className="header-content">Dashboard</h1>
        <div className="div-content">
          <p>  <Link to="/login">Click</Link> here to go to LogIn!</p>
           <p> <Link to="/home">Click</Link> here to go to Home! </p>
        </div>
        <div className="div-content">
            <h3>Nested Routing example:</h3>
           
         <Row>
      
             <Col md={12}>
                <Link to="/dashboard/menu"> <Button type="link">Menu</Button></Link>
             </Col>
             <Col md={12}><Link to="/dashboard/profile"> <Button type="link">Profile</Button></Link>
            </Col>
         
         </Row>
         <Routes> <Route path="/menu" element={<MenuPage />} />
        <Route path="/profile/*" element={<Profile />} />  </Routes>
        </div>
    </div>

  
    )
}

export default Dashboard
