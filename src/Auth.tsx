import React from 'react'
import { Link } from "react-router-dom";

const Auth = () => {
    return (
        <div className="div-content">
            <h1 className="header-content">Login</h1>
            <div className="div-content">
              <p>  <Link to="/home">Click</Link> here to go to home!</p>
               <p> <Link to="/dashboard">Click</Link> here to go to Dashboard! </p>
            </div>
        </div>
    )
}

export default Auth
