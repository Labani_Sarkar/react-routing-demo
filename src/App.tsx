
import './App.css';
import { BrowserRouter as Router, Route, Routes , Navigate } from "react-router-dom";
import Auth from './Auth';
import Dashboard from './dashboard/Dashboard';
import Home from './Home';

function App() {
  return (
    <Router>
 <Routes>
 <Route path="/" element={<Navigate to ="/login" />}/>

    <Route path="/login" element={<Auth />} />
    <Route path="/home" element={<Home />} />
    <Route path="/dashboard/*" element={<Dashboard />} />
    </Routes>
    
      {/* <Route path="/news">
        <NewsFeed />
      </Route> */}
  </Router>
  );
}

export default App;
