import React from 'react'
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <div className="div-content">
        <h1 className="header-content">Home</h1>
        <div className="div-content">
          <p>  <Link to="/login">Click</Link> here to go to LogIn!</p>
           <p> <Link to="/dashboard">Click</Link> here to go to Dashboard! </p>
        </div>
    </div>
    )
}

export default Home
